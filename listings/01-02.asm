
  ;; ACME Assembler
  !to "01-02.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  CLD
  CLC       ; Gets the 6510 ready for adding.
  LDA #1    ; LoaD "1" into the Accumulator in IMmediate mode.
  STA 1024  ; STore the contents of Accumulator (1) in 1024.
  STA 55296 ; STore the contents of Accumulator (1) in 55296
  LDA #2    ; LoaD "2" into the Accumulator in IMmediate mode.
  ADC 1024  ; ADd Contents of 1024 (1) to the contents of the accumulator (2).
  STA 1026  ; STore the contents ol the Accumulator (3) in 1026.
  STA 55298 ; STore the contents of the Accumulator (3) in 55298.
  RTS       ; ReTurn from machine-code Subroutine.

