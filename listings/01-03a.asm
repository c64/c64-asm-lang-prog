
  ;; ACME Assembler
  !to "01-03a.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #1     ; Load '1' into the accumulator
  STA 1024   ; Store the contents of accumulator in 1024
  STA 55296  ; Store the contents of accumulator in 55296 to give the screen display a colour (1 gives white)
  LDY 1024   ; Load into X-register, contents of memory location 1024 (i.e. "1")
  STY 1026   ; Store contents of X-register in 1026 (i.e. 1)
  STY 55298  ; Store contents of X-register in 55298 to make the character placed on the screen white
  RTS        ; Return from machine-code subroutine

