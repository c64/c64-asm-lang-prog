
  ;; ACME Assembler
  !to "01-04.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #1
  STA 1024
  STA 55296
  TAX
  STX 1026
  STX 55298
  RTS

