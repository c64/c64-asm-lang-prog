
  ;; ACME Assembler
  !to "02-02.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #1             ; Load Accumulator in immediate mode with a '1' o
  JSR 834            ; Jump to the subroutine at 834
  RTS                ; Return from subroutine (i.e. back to BASIC)
  STA 1024           ; Store contents of accumulator in 1024
  STA 55296          ; Store accumulator in 55296 :
  RTS                ; Return from subroutine

