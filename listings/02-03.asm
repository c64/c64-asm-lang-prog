
  ;; ACME Assembler
  !to "02-03.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828                       ; 828 decimal

  LDX #100
  DEX
  BEQ 836                       ; $0344 is where stx 1024 is located
  JMP 830
  STX 1024
  STX 55296
  RTS

