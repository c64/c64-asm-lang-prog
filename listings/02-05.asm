
  ;; ACME Assembler
  !to "02-05.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #90             ; Load 90 (a diamond) into Accumulator.
  STA 890             ; Store contents of Accumulator in 890 
  LDX #0              ; Load '0' into X register
  INX                 ; Increment X register
  CPX 890             ; Compare value in X register with that in 890 (i.e. 90)
  BEQ 844             ; Branch forward three bytes if CPX answer=0
  JMP 835             ; Jump to memory location 835
  STX 1024            ; Store contents of X in 1024
  LDA #1              ; Load a one into Accumulator
  STA 55296           ; Store it in colour RAM to give a white image on the screen
  RTS                 ; Return from machine code to BASIC.

