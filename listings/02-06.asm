
  ;; ACME Assembler
  !to "02-06.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDX #0              ; Load a '00' into X.
  LDA #83             ; Load an '83' (a heart) into A
  INX                 ; Increment X
  STX 900             ; Store X in 900
  CMP 900             ; Compare A with 900
  BNE 832             ; Branch if Not Equal
  STX 1024            ; Store X in 1024
  LDA #1              ; Load a '1' into the Accumulator
  STA 55296           ; Store in colour RAM to get white display
  RTS                 ; Return from Subroutine
