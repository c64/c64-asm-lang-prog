
  ;; ACME Assembler
  !to "02-07.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #42
  LDY #100
  DEY
  BNE 832                       ; changed jump address (original 253)
  STA 1024
  LDA #1
  STA 55296
  RTS
