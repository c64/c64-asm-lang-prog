
  ;; ACME Assembler
  !to "02-08.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #90            ; Load 90 into A
  STA 900            ; Store Accumulator contents in 900,
  LDY #0             ; Load 'O' into Y
  INY                ; Increment Y
  CPY 900            ; Compare contents of 900 with contents of Y
  BMI 835            ; Branch on minus, i.e. test N flag
  STY 1024           ; Store contents Y in 1024
  LDA #1             ; Load Accumulator with '1'
  STA 55296          ; Store in colour RAM to get white display
  RTS                ; Return from subroutine
