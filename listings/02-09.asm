
  ;; ACME Assembler
  !to "02-09.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA #91
  STA 900
  LDY #100
  DEY
  CPY 900
  BPL 835                       ; 250 for ACME
  STY 1024
  LDA #7
  STA 55296
  RTS
