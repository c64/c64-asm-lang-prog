
  ;; ACME Assembler
  !to "03-01.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDX #100           ; Load 100 into X
  LDA #90            ; Load 90 (a diamond) into A
  STA 1023,X         ; Output diamond at (1023+X)
  LDA #1             ; Load 1 into A
  STA 55295,X        ; Ensure colour is white
  DEX                ; Decrement X value
  BNE 830            ; Branch on not equal (original 243)
  RTS
