
  ;; ACME Assembler
  !to "03-02.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDX #216
  LDA #42                       ; (memory position 830)
  STA 808,X
  LDA #1
  STA 55080,X
  INX
  BNE 830                       ; (original 243)
  RTS
