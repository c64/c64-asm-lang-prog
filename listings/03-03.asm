
;;; Commodore 64 Assembly Language Programming
  
;;; As with BASIC programs a character can be moved across the screen
;;; by filling the screen with the character while POKING a blank one
;;; space behind it. Program 3-3 demonstrates this type of routine.

  
  ;; ACME Assembler
  !to "03-03.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828                       ; start at the beginning of tape buffer

  LDX #0
  LDY #32
  STY 900
  LDA #90
  STA 901
  STA 1024,X                    ; (memory position 840)
  LDA #1
  STA 55296,X
  TYA
  STA 1023,X
  LDA 901
  INX
  BNE 840                       ; (original 238)
  RTS

