
  ;; ACME Assembler
  !to "03-04.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDX #250           ; 2 cycles
  DEX                ; 2 cycles
  BNE 830            ; 3 cycles
  RTS
