
  ;; ACME Assembler
  !to "03-05.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDY #200           ; 2 cycles
  LDX #250           ; 2 cycles
  DEX                ; 2 cycles
  BNE 832            ; 3 cycles
  DEY                ; 2 cycles
  BNE 830            ; 3 cycles
  RTS
