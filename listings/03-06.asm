
;;; Program 3.6 uses 3.3 as a basis and inserts the delay loop illustrated
;;; in 3.4 putting 0.6 milliseconds between the appearance and
;;; disappearance of a diamond.

;;; labels are used in this version to avoid having to hardcode the
;;; memory locations for BNE statements

  ;; ACME Assembler
  !to "03-06.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDY #0
  LDA #90            ; Set up a diamond
  STA 900
  LDA #1             ; Set up colour white
  STA 901
  LDA #32            ; Set up a blank
  STA 902

loop1
  LDX #250           ; Set up for delay (outer loop)
  LDA 900            ; Load and Display diamond
  STA 1024,Y
  LDA 901            ; Load colour white
  STA 55296,Y

loop2
  DEX                ; Delay loop (inner loop)
  BNE loop2					 ; (original 253)
  LDA 902            ; Load blank into accumulator
  STA 1024,Y         ; Display blank
  INY                ; Set up to process next location on the screen
  BNE loop1          ; (original 230)
  RTS
