
;;; Program 3.6 uses 3.3 as a basis and inserts the delay loop illustrated
;;; in 3.4 putting 0.6 milliseconds between the appearance and
;;; disappearance of a diamond.

;;; labels are used in this version to avoid having to hardcode the
;;; memory locations for BNE statements

  ;; ACME Assembler
  !to "03-06a.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDY #0
  LDA #90            ; Set up a diamond
  STA 900
  LDA #1             ; Set up colour white
  STA 901
  LDA #32            ; Set up a space
  STA 902
  LDA 900            ; Load a diamond
  STA 1024,Y         ; Display it on the screen
  LDA 901            ; Load colour white
  STA 55296,Y        ; Store in colour RAM
  STY 903            ; Save Y register during...
  LDY #15            ; Set outer loop
  LDX #250           ; Set inner loop

loop1
  DEX
  BNE loop1          ; Count down 250 times

loop2
  DEY
  BNE loop2          ; Count down 15

  LDY 903            ; Restore Y register as screen index
  LDA 902            ; Load blank
  STA 1024,Y         ; Display blank
  INY                ; Set up for next screen location
  BNE loop1          ; Loop unless all done
  RTS
