
  ;; ACME Assembler
  !to "03-07.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA 162     ; Load accumulator in Zero-page mode with contents of 160
  STA 1024    ; Store contents of A in 1024
  LDA #1      ; Load accumulator with 1
  STA 55296   ; Store in colour RAM
  RTS         ; Return from machine code subroutine
