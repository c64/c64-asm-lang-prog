
  ;; ACME Assembler
  !to "03-08.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDY #0             ; Load Y with '0'
  TYA                ; Transfer Y to A
  INY                ; Increment Y
  STA 1023,Y         ; Store contents of A in 1023+Y
  LDA #1             ; Load A with 1
  STA 55295,Y        ; Store in 55295+Y
  CPY #100           ; Compare Y with 100
  BNE 830            ; Branch if Z flag not set
  RTS                ; Return from machine code subroutine
