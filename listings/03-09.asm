
  ;; ACME Assembler
  !to "03-09.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDX #0             ; Load X immediate with '0'
  LDA (84,X)         ; Load A indirect 84+X
  STA 1024,X         ; Store A in 1024+X
  LDA #1             ; Load A with 1
  STA 55296,X        ; Store in colour RAM
  INX                ; Increment X
  CPX #4             ; Compare X immediate with '4'
  BNE 830            ; Branch if not equal
  RTS
