
  ;; ACME Assembler
  !to "03-10.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828

  LDA (90,X)
  LDX #51
  STX 844
  LDX #03
  STX 845
  JMP 844
  NOP
  NOP
  NOP
  NOP
  NOP
  NOP
  STA 1024
  LDA #1
  STA 55296
  RTS
