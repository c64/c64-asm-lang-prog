
;;; Commodore 64 Assembly Language Programming
	
;;; Controlling the Colour
;;; 
;;; Program 6.6 shows how the screen/border combination can be
;;; demonstrated using a short machine code program.
	
  ;; ACME Assembler
  !to "06-06.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	LDY #15
	LDX #15
	
SCREEN
 STY $D021
	
BORDER
	STX $D020

	DEX
	BPL BORDER
	DEY
	BPL SCREEN
	RTS

