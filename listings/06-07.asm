
;;; Commodore 64 Assembly Language Programming
	
;;; Controlling the Colour
;;; 
;;; In previous chapters we used counting loops to produce the delay,
;;; sometimes nested loops. The next program does things the easy way by
;;; making use of the three byte jiffy clock at 160-162 (A0-A21g). This is
;;; a binary counter which counts jiffies (i/60ths of a second), 162
;;; (A216) is incremented by 1 every jiffy, and rolls over into 161 (A116)
;;; every 256 jiffies, 161(A116> rolls over into i6O(A21g) every 65536
;;; jiffies. This gives ample scope- for delays. Program 6.2 loads 246
;;; into 162 and waits for it to become positive which will happen in
;;; approximately 1/6 second

  ;; ACME Assembler
  !to "06-07.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	LDY #15												; load y register with 15

BORDER
	STY $D020											; store value of y in location $D020
	LDX #15												; load x register with 15

SCREEN
	STX $D021											; store value of x in $D021
	LDA #246											; load ACC with 246
	STA 162												; store value of a in 162

LOOP
	LDA 162												; load ACC with value at address 162
	BMI LOOP											; branch if result is minus
	DEX														; decrement x
	BNE SCREEN										; branch on result not zero

	DEY														; decrease y
	BNE BORDER										; brance on result not zero
	RTS

