
;;; Commodore 64 Assembly Language Programming
	
;;; 	Program 6.8 shows how the colour of blocks of screen can be
;;; 	defined by means of  the screen color codes
	
  ;; ACME Assembler
  !to "06-08.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	ldx #200

loop
	lda #8
	sta $d7ff,x
	lda #160
	sta 1023,x
	dex
	bne loop
	rts
