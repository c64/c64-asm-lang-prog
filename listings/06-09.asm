
;;; Commodore 64 Assembly Language Programming
	
;;; Program 6.8 shows how the colour of blocks of screen can be
;;; defined by means of the screen color codes. This  process is
;;; taken a stage further in the program 6.9 where the sixteen
;;; different colours are cycled through, with a delay between each.
	
  ;; ACME Assembler
  !to "06-09.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	lda #0												; set up first colour and
	sta 1019											; save  at top of tape buffer

loop1
	ldx #200											; set counter for 10 rows

loop2
	lda #160											; load reversed space for screen

	sta 1023,x										; print block onto screen
	lda 1019
	sta $d7ff,x
	dex
	bne loop2

	lda #186
	sta 162

delay
	lda 162
	bmi delay

	lda 1019
	adc #1
	cmp #16
	sta 1019
	bne loop1
	rts
	
