
;;; Commodore 64 Assembly Language Programming
	
;;; 	Program 6.8 shows how the colour of blocks of screen can be
;;; 	defined by means of  the screen color codes
	
  ;; ACME Assembler
  !to "07-01.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	lda #42												; load ACC with char 42 (asterisk)
	sta 1524											; store in the middle of the screen
	ldx #1												; load x register with 1 for colour white
	stx 55796											; store value of x in color ram
	jsr $ffd2											; jump to CHROUT subroutine
	rts
	
