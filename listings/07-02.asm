
;;; Commodore 64 Assembly Language Programming
	
;;; Let us use the plot routine to place a yellow asterisk
;;; at the beginning of the thenth line of the screen
	
  ;; ACME Assembler
  !to "07-02.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	clc														; set carry register to move cursor
	ldx #9												; load the x register with 9 (10th row)
	ldy #0												; load the y register with 0 (first column)
	jsr $fff0											; call plot routine to position cursor
	lda #7												; load 7 (yellow) into ACC
	sta 646												; make this the current foreground color
	lda #42												; load asterisk char value
	jsr $ffd2											; output to screen routine
	
	lda #14												; reset forground color to light blue
	sta 646
	rts
