
;;; Commodore 64 Assembly Language Programming
	
;;; Let us use the plot routine to place a yellow asterisk
;;; at the beginning of the thenth line of the screen
	
  ;; ACME Assembler
  !to "07-04.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	jsr $e544											; clear screen

	lda #0												; set ACC clear

loop
	jsr $fff4											; jump to GETIN subroutine
	beq loop											; branch back if no input given
	jsr $ffd2											; display ACC value as char on screen
	
	rts
