
;;; Commodore 64 Assembly Language Programming
	
  ;; ACME Assembler
  !to "07-05.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	jsr $ffcf											; call CHRIN routine
	rts

	
