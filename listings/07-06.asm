
;;; Commodore 64 Assembly Language Programming
	
  ;; ACME Assembler
  !to "07-06.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	ldx #44												; store terminator in
	stx 900												; address 900 $384
	
loop
	jsr $ffe4											; use GETIN subroutine
	beq loop 											; wait for input

	jsr $ffd2											; output on screen

	cmp 900												; look for a comma, if not present
	bne loop											; branch back

	lda #13												; output a <return> to
	jsr $ffd2											; screen to be tidy

	rts

	
