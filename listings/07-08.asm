
;;; Commodore 64 Assembly Language Programming
	
  ;; ACME Assembler
  !to "07-08.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	ldx #4												; set up loop counter

loop
	jsr $ffe4											; GETIN subroutine
	beq loop
	jsr $ffd2											; CHROUT subroutine
	dex
	bne loop											; check for end of loop
	
	rts

	
