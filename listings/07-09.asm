
;;; Commodore 64 Assembly Language Programming
	
  ;; ACME Assembler
  !to "07-09.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	ldx #4

loop
	jsr $ffe4
	beq loop
	stx 1024
	ldy #1
	sty 55296
	jsr $ffd2
	stx 1026
	ldy #1
	sty 55298
	dex
	bne loop
	
	rts

	
