
;;; Commodore 64 Assembly Language Programming
	
  ;; ACME Assembler
  !to "07-10.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	loopconst = 900

	ldx #4
store
	stx loopconst
get
	jsr $ffe4
	beq get
	jsr $ffd2
	ldx loopconst
	dex
	bne store
	
	rts

	
