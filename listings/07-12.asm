
;;; Commodore 64 Assembly Language Programming
	
  ;; ACME Assembler
  !to "07-12.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = 828 											; start at the beginning of tape buffer

	ldx #4

loop
	txa
	pha
	jsr $ffe4
	beq 251
	jsr $ffd2
	pla
	tax
	dex
	bne 241

	rts
